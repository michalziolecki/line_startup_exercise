# Java Puzzel Task #

#### Exercise for Sii ####

### Project Evn ###
* Project created in Intellij

### Requirements ###
* Java 1.8+ for compilation
* Cmd/Bash/GitBash for run Jar from console with arguments

### Run Jar from console ###
* Program has name sii_exercise.jar in root of project and at the end of 'out/' path
* Command to run jar:

	`java -jar sii_exercise`

 * Arguments:
 
   * `-d, --diodes Number of diodes`
   
   * `-e, --employee Number of employees`
   
   * `-h, --help Help`

### Example output ###
`~/sii_exercise (master)$ java -jar sii_exercise.jar -e 4 -d 150`

`Sii Exercise solution:`

 ` Process run!`
 
 ` PCLs run number: 88`
 
 ` Process stop!`

### Compile and Tests ###
* Use Intellij IDE to compile code and run unit tests

