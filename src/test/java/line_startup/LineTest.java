package line_startup;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

class LineTest {

    @ParameterizedTest
    @MethodSource("prepareAssemblyProcess")
    void assemblyProcessTest(Integer employees, Integer diodes, Integer result) throws UnknownActionException {
        Line line = new Line(employees, diodes);
        line.initProcess();
        try {
            line.assemblyProcess();
        } catch (UnknownActionException e) {
            Assertions.fail();
        }
        Assertions.assertEquals(result, line.getPlcsInRunState());
    }

    private static Stream<Arguments> prepareAssemblyProcess() {
        return Stream.of(
                Arguments.of(2, 8, 4),
                Arguments.of(3, 8, 4),
                Arguments.of(4, 8, 6),
                Arguments.of(8, 2, 1)
        );
    }

    @Test
    void switchStateDiodeForEmployeeTest() throws UnknownActionException {
        // given
        List<DiodePCL> diodes = new ArrayList<>(Arrays.asList(new DiodePCL(1, PCLState.RUN),
                new DiodePCL(2, PCLState.RUN),
                new DiodePCL(3, PCLState.PROGRAM),
                new DiodePCL(4, PCLState.RUN)));

        // when
        Line.switchStateDiodeForEmployee(new Employee(2), diodes);

        // then
        Assertions.assertEquals(1, Line.findRunDiode(diodes).size());
    }

    @Test
    void switchStateDiodeForEmployeeExceptionTest() {
        // given
        List<DiodePCL> diodes = new ArrayList<>(Arrays.asList(new DiodePCL(1, PCLState.RUN),
                new DiodePCL(2, PCLState.RUN),
                new DiodePCL(3, PCLState.PROGRAM),
                new DiodePCL(4, PCLState.OFF)));

        // when/then
        Assertions.assertThrows(UnknownActionException.class, () -> {
            Line.switchStateDiodeForEmployee(new Employee(2), diodes);
        });
    }

    @ParameterizedTest
    @MethodSource("prepareDiodesList")
    void findRunDiodeTest(List<DiodePCL> input, Integer result) {
        List<DiodePCL> filteredList = Line.findRunDiode(input);
        Assertions.assertEquals(result, filteredList.size());
    }

    private static Stream<Arguments> prepareDiodesList() {
        return Stream.of(
                Arguments.of(new ArrayList<>(Arrays.asList(new DiodePCL(1),
                        new DiodePCL(2),
                        new DiodePCL(3),
                        new DiodePCL(4))), 0),
                Arguments.of(new ArrayList<>(Arrays.asList(new DiodePCL(1, PCLState.RUN),
                        new DiodePCL(2, PCLState.RUN),
                        new DiodePCL(3, PCLState.PROGRAM),
                        new DiodePCL(4, PCLState.OFF))), 2),
                Arguments.of(new ArrayList<>(Arrays.asList(
                        new DiodePCL(1, PCLState.PROGRAM),
                        new DiodePCL(2, PCLState.RUN),
                        new DiodePCL(3, PCLState.PROGRAM),
                        new DiodePCL(4, PCLState.RUN))), 2)
        );
    }
}