package line_startup;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


public class Line {
    @Setter
    private int employeesNumber;
    @Setter
    private int plcsNumber;
    @Getter
    private Integer plcsInRunState;
    private List<DiodePCL> diodesList;
    private List<Employee> employeeList;


    public Line() {
        this.employeesNumber = 1100;
        this.plcsNumber = 1100;
        this.plcsInRunState = 0;
        this.diodesList = new ArrayList<>();
        this.employeeList = new LinkedList<>();
    }

    public Line(Integer employeesNumber, Integer plcsNumber) {
        this.employeesNumber = employeesNumber;
        this.plcsNumber = plcsNumber;
        this.plcsInRunState = 0;
        this.diodesList = new ArrayList<>();
        this.employeeList = new LinkedList<>();
    }

    public void initProcess() {
        prepareDiodes();
        prepareEmployees();
    }

    private void prepareDiodes() {
        for (int i = 1; i <= this.plcsNumber; i++) {
            this.diodesList.add(new DiodePCL(i));
        }
    }

    private void prepareEmployees() {
        for (int i = 1; i <= this.employeesNumber; i++) {
            this.employeeList.add(new Employee(i));
        }
    }

    public Integer assemblyProcess() throws UnknownActionException {
        for (Employee employee : employeeList) {
            if (employee.getId() == 1) {
                initAllDiodes();
                continue;
            }
            if (employee.getId() > diodesList.size()) break;
            switchStateDiodeForEmployee(employee);
        }
        this.plcsInRunState = findRunDiode().size();
        return this.plcsInRunState;
    }

    public void switchStateDiodeForEmployee(Employee employee) throws UnknownActionException {
        switchStateDiodeForEmployee(employee, this.diodesList);
    }

    public static void switchStateDiodeForEmployee(Employee employee, List<DiodePCL> diodes) throws UnknownActionException {
        for (int index = employee.getId() - 1;
             index < diodes.size();
             index += employee.getId()) {

            DiodePCL diode = diodes.get(index);
            switch (diode.getState()) {
                case RUN:
                    diode.setState(PCLState.PROGRAM);
                    break;
                case PROGRAM:
                    diode.setState(PCLState.RUN);
                    break;
                default:
                    throw new UnknownActionException(diode.getState());
            }
        }
    }

    public List<DiodePCL> findRunDiode() {
        return findRunDiode(this.diodesList);
    }

    public static List<DiodePCL> findRunDiode(List<DiodePCL> diodes) {
        return diodes.stream()
                .filter(diode -> diode.getState() == PCLState.RUN)
                .collect(Collectors.toList());
    }

    private void initAllDiodes() {
        for (DiodePCL diode : diodesList) {
            diode.setState(PCLState.RUN);
        }
    }
}
