package line_startup;

enum PCLState {
    OFF(0),
    RUN(1),
    PROGRAM(2);

    private int stateVal;

    PCLState(int stateVal) {
        this.stateVal = stateVal;
    }

    public int getStateVal() {
        return this.stateVal;
    }
}
