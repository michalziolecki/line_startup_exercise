package line_startup;

public class UnknownActionException extends Exception {
    UnknownActionException(PCLState state) {
        super("Unknown action for state " + state.toString() + " !");
    }
}
