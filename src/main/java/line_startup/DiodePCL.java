package line_startup;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DiodePCL {
    int id;
    PCLState state;

    public DiodePCL(Integer id){
        this.id = id;
        this.state = PCLState.OFF;
    }

}
