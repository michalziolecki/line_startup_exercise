import line_startup.Line;
import line_startup.UnknownActionException;
import org.apache.commons.cli.*;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {

        Optional<CommandLine> optionalArgs = ArgsParser.parseInput(args);
        Line line;
        if (optionalArgs.isPresent()){
            line = ArgsParser.prepareProcessLineByArgs(optionalArgs.get());
        } else{
            line = new Line();
        }
        line.initProcess();
        System.out.println("Sii Exercise solution: \n Process run!");
        try {
            line.assemblyProcess();
        } catch (UnknownActionException ex) {
            System.out.println(" Program stop by " + ex.getMessage());
            System.exit(1);
        }
        System.out.println(" PCLs run number: " + line.getPlcsInRunState());
        System.out.println(" Process stop!");
    }
}
