import line_startup.Line;
import org.apache.commons.cli.*;

import java.util.Optional;

public class ArgsParser {
    public static Line prepareProcessLineByArgs(CommandLine cmd) {
        Line line = new Line();
        if (cmd.hasOption("h")) {
            System.out.println(" Sii Exercise solution! \n" +
                    "  -d, --diodes Number of diodes \n" +
                    "  -e, --employee Number of employees \n" +
                    "  -h, --help Help \n");
            System.exit(0);
        }
        if (cmd.hasOption("e")) {
            try {
                line.setEmployeesNumber(Integer.parseInt(cmd.getOptionValue("e")));
            } catch (NumberFormatException ex) {
                System.out.println("Bad input parameters! \n " + ex.getMessage());
                System.exit(1);
            }

        }
        if (cmd.hasOption("d")) {
            try {
                line.setPlcsNumber(Integer.parseInt(cmd.getOptionValue("d")));
            } catch (NumberFormatException ex) {
                System.out.println("Bad input parameters! \n " + ex.getMessage());
                System.exit(1);
            }

        }
        return line;
    }

    public static Optional<CommandLine> parseInput(String[] args) {
        Options options = new Options();

        Option employees = new Option("e", "employees", true, "Number of employees");
        employees.setRequired(false);

        Option diodes = new Option("d", "diodes", true, "Number of diodes");
        diodes.setRequired(false);

        Option help = new Option("h", "help", false, "Help");
        help.setRequired(false);

        options.addOption(employees);
        options.addOption(diodes);
        options.addOption(help);

        CommandLineParser parser = new BasicParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp(" ", options);
            System.exit(1);
        }

        return Optional.of(cmd);
    }
}
